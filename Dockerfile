# syntax=docker/dockerfile:1
FROM python:3.8-slim-bullseye

ENV USER=boggle
ENV HOME=/app

ENV POETRY_VERSION=1.2.0b3

WORKDIR $HOME

RUN set -aeux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
		curl \
	; \
	rm -rf /var/lib/apt/lists/* ; \
    useradd -M $USER && \
    chown -R $USER $HOME

USER $USER

RUN python3 -m venv $HOME && \
    $HOME/bin/pip install setuptools poetry==$POETRY_VERSION

COPY ./ $HOME

RUN $HOME/bin/poetry install

CMD $HOME/bin/poetry run gunicorn --workers 1 --bind 0.0.0.0 src.boggle_web.main:app

HEALTHCHECK CMD curl --fail localhost:8000/status || exit 1

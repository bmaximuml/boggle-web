#!/usr/bin/env python3

from boggle import board
from datetime import datetime
from flask import Flask, render_template, request
from os import environ
from secrets import token_hex
from werkzeug.exceptions import BadRequestKeyError

VARS = [
    ('FLASK_SECRET_KEY', token_hex(64)),
    ('TITLE', 'Letter Shuffle Game'),
    ('AUTHOR', 'Max Levine')
]

def read_file(file):
    with open(file, 'r') as f:
        result = f.read().strip()
        return result

def create_application():
    application = Flask(__name__)

    for var, default in VARS:
        try:
            application.config[var.lower()] = read_file(environ[f'{var}_FILE'])
        except (KeyError, FileNotFoundError):
            try:
                application.config[var.lower()] = environ[var]
            except KeyError:
                application.config[var.lower()] = default

    return application


app = create_application()


@app.route('/', methods=['GET'])
def home():
    try:
        seed = str(request.args['seed']).lower().strip()
    except BadRequestKeyError:
        seed = None
    b = board(seed)
    split_board = [b[:4], b[4:8], b[8:12], b[12:]]

    return render_template(
        'index.html',
        year=datetime.now().year,
        board=split_board,
        seed=seed,
        title=app.config['title'],
        author=app.config['author'],
    )

@app.route('/status')
def status():
    return 'Active'

if __name__ == '__main__':
    app.debug = True
    app.run()
